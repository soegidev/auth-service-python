import os
import psycopg2
from flask import Flask, render_template

app = Flask(__name__)

conn = psycopg2.connect(host='103.187.146.183',
                            database='db_user_module_prod',
                            user=os.environ['DB_USERNAME'],
                            password=os.environ['DB_PASSWORD'])

cur = conn.cursor()

# CREATE TABLE
cur.execute('DROP TABLE IF EXISTS public.user_account cascade;')
cur.execute('CREATE TABLE public.user_account ('
            'id varchar(100) NOT NULL, '
            'firstname varchar(100) NULL, '
	        'lastname varchar(100) NULL, '
	        'gender bpchar(1) NULL, '
	        'dateofbirth date NULL, '
	        'role_id int4 NOT NULL, '
	        'deleted bool NULL DEFAULT false, '
	        'createddate timestamp NULL, '
	        'updateddate timestamp NULL DEFAULT CURRENT_TIMESTAMP, '
	        'CONSTRAINT user_account_pkey PRIMARY KEY (id));')

cur.execute('DROP TABLE IF EXISTS public.user_roles cascade;')
cur.execute('CREATE TABLE public.user_roles ('
            'id serial4 NOT NULL,'
	        'description varchar(100) NULL,'
	        'createddate timestamp NULL,'
	        'CONSTRAINT user_roles_pkey PRIMARY KEY (id));')

cur.execute('ALTER TABLE public.user_account ADD CONSTRAINT ' 
            'fk_user_account_roles foreign key (role_id) REFERENCES '
            'public.user_roles(id) ON DELETE CASCADE ON UPDATE CASCADE;')

conn.commit()

cur.execute('DROP TABLE IF EXISTS public.email_validation_status cascade;')

cur.execute('CREATE TABLE public.email_validation_status ( '
	        'id serial4 NOT NULL, '
	        'description varchar(20) NOT NULL, '
	        'createddate timestamp NULL, '
	        'updateddate timestamp NULL DEFAULT CURRENT_TIMESTAMP, '
	        'CONSTRAINT email_validation_status_pkey PRIMARY KEY (id));')

cur.execute('DROP TABLE IF EXISTS public.external_providers cascade;')

cur.execute('CREATE TABLE public.external_providers ( '
	        'id serial4 NOT NULL, '
	        'providename varchar(50) NULL, '
	        'wsendpoint varchar(100) NULL, '
	        'createddate timestamp NULL DEFAULT CURRENT_TIMESTAMP, '
	        'CONSTRAINT external_providers_pkey PRIMARY KEY (id));')
conn.commit()
cur.execute('DROP TABLE IF EXISTS public.hashing_algorithms cascade;')
cur.execute('CREATE TABLE public.hashing_algorithms ( '
	        'id int4 NOT NULL, '
	        'description varchar(20) NOT NULL, '
	        'createddate timestamp NULL, '
	        'updateddate timestamp NULL DEFAULT CURRENT_TIMESTAMP, '
	        'CONSTRAINT hasing_algorithms_pkey PRIMARY KEY (id));')


cur.execute('DROP TABLE IF EXISTS public.user_login_data')
cur.execute('CREATE TABLE public.user_login_data ( '
	        'id varchar(100) NOT NULL, '
	        'username varchar(20) NOT NULL, '
	        'password_hash varchar(250) NULL, '
	        'password_salt varchar(100) NULL, '
	        'hashalgorithm_id int4 NOT NULL, '
	        'emailaddress varchar(100) NOT NULL, '
	        'confirmation_token varchar(100) NULL, '
	        'token_confirmation_time timestamp NULL, '
	        'email_validation_status_id int4 NOT NULL, '
	        'password_recovery_token varchar(100) NULL, '
	        'recovery_token_time timestamp NULL, '
	        'createddate timestamp NULL, '
	        'updateddate timestamp NULL DEFAULT CURRENT_TIMESTAMP, '
	        'CONSTRAINT user_login_data_pkey PRIMARY KEY (id), '
	        'CONSTRAINT user_login_data_username_emailaddress_key UNIQUE (username, emailaddress));')


cur.execute('ALTER TABLE public.user_login_data '
            'ADD CONSTRAINT fk_user_login_data_email_validation '
            'FOREIGN KEY (email_validation_status_id) REFERENCES '
            'public.email_validation_status(id) ON DELETE CASCADE '
            'ON UPDATE CASCADE;')
cur.execute('ALTER TABLE public.user_login_data '
            'ADD CONSTRAINT fk_user_login_data_hashing_algorithm '
            'FOREIGN KEY (hashalgorithm_id) REFERENCES ' 
            'public.hashing_algorithms(id);')
cur.execute('ALTER TABLE public.user_login_data '
            'ADD CONSTRAINT fk_user_login_data_id '
            'FOREIGN KEY (id) REFERENCES public.user_account(id) '
            'ON DELETE CASCADE ON UPDATE CASCADE;')
conn.commit()

cur.execute('DROP TABLE IF EXISTS public.user_login_data_external cascade;')
cur.execute('CREATE TABLE public.user_login_data_external ( '
	        'id varchar(100) NOT NULL, '
	        'externalprovideid int4 NOT NULL, '
	        'externalprovidetoken varchar(100) NULL, '
	        'createddate timestamp NULL, '
	        'updateddate timestamp NULL DEFAULT CURRENT_TIMESTAMP, '
	        'CONSTRAINT user_login_data_external_externalprovideid_key UNIQUE (externalprovideid), '
	        'CONSTRAINT user_login_data_external_pkey PRIMARY KEY (id));')

cur.execute('ALTER TABLE public.user_login_data_external '
            'ADD CONSTRAINT fk_user_login_data_external_login '
            'FOREIGN KEY (id) REFERENCES public.user_account(id) '
            'ON DELETE CASCADE ON UPDATE CASCADE;')


cur.execute('ALTER TABLE public.user_login_data_external '
            'ADD CONSTRAINT fk_user_login_data_external_providers '
            'FOREIGN KEY (externalprovideid) REFERENCES '
            'public.external_providers(id) '
            'ON DELETE CASCADE ON UPDATE CASCADE;')
conn.commit()

# INSERT DATA
cur.execute("INSERT INTO public.user_roles "
            "(description,createddate) VALUES "
            "('administrator',NULL), " 
	        "('owner',NULL), "
        	"('user',NULL), "
	        "('author',NULL), "
	        "('editor',NULL), "
	        "('superuser',NULL);")

cur.execute("INSERT INTO public.email_validation_status "
            "(description,createddate,updateddate) VALUES "
	        "('valid','2022-07-18 10:02:10.572499','2022-07-18 03:02:10.570138'), "
	        "('invalid','2022-07-18 10:02:10.736419','2022-07-18 03:02:10.739124'), "
	        "('risky','2022-07-18 10:02:10.826589','2022-07-18 03:02:10.831581'), "
	        "('unverifiable','2022-07-18 10:02:10.947222','2022-07-18 03:02:10.943099'), "
	        "('unknown','2022-07-18 10:02:11.051366','2022-07-18 03:02:11.054588'), "
	        "('corrected','2022-07-18 10:02:11.151636','2022-07-18 03:02:11.152848');")

cur.execute("INSERT INTO public.hashing_algorithms "
            "(id,description,createddate,updateddate) VALUES "
	        "(1,'HS256','2022-07-18 10:32:41.792162','2022-07-18 03:32:41.783946'), "
	        "(2,'HS384','2022-07-18 10:32:42.067152','2022-07-18 03:32:42.048518'), "
	        "(3,'HS512','2022-07-18 10:32:42.183841','2022-07-18 03:32:42.158537'), "
	        "(4,'ES256','2022-07-18 10:38:14.733294','2022-07-18 03:38:14.655899'), "
	        "(5,'ES256K','2022-07-18 10:52:56.750983','2022-07-18 03:52:56.101473'), "
	        "(6,'ES384','2022-07-18 11:04:33.037555','2022-07-18 04:04:33.020509'), "
	        "(7,'ES512','2022-07-18 11:06:32.745882','2022-07-18 04:06:32.735312'), "
	        "(8,'RS256','2022-07-18 11:13:45.264091','2022-07-18 04:13:45.09977'), "
	        "(9,'RS384','2022-07-18 11:17:07.578121','2022-07-18 04:17:07.56226'), "
	        "(10,'RS512','2022-07-18 11:19:53.596073','2022-07-18 04:19:53.554965'), "
	        "(11,'PS256','2022-07-18 11:31:48.342771','2022-07-18 11:31:48.335459');")

cur.execute("INSERT INTO public.user_account "
            "(id,firstname,lastname,gender,dateofbirth,"
            "role_id,deleted,createddate,updateddate) VALUES "
    	    "('cdc0d1d5-fd05-4f52-bb60-fa7fdb7aea3f','Soegi','Admin Soegi',"
            "'m','1988-01-19',1,false,'2022-09-03 10:57:49.315629',"
            "'2022-09-18 08:24:22.417492');")

cur.execute("INSERT INTO public.user_login_data "
            "(id,username,password_hash,password_salt,hashalgorithm_id,"
            "emailaddress,confirmation_token,token_confirmation_time,"
            "email_validation_status_id,password_recovery_token,"
            "recovery_token_time,createddate,updateddate) VALUES "
	        "('cdc0d1d5-fd05-4f52-bb60-fa7fdb7aea3f','soegidev',"
            "'$2b$12$FEZ1zSRNE8mKAQtS6/ZVROIunRWXEo.hTO8vV7ipHyzHeNQM/TVTC',"
            "'$2b$12$FEZ1zSRNE8mKAQtS6/ZVRO',1,'soegidev.id@gmail.com',"
            "'InNvZWdpZGV2LmlkQGdtYWlsLmNvbSI.Yxqtyg.yDlWZPpyDsZ8EOqgI1_4H3KzhJs',"
            "'2022-09-09 10:10:52.604889',1,"
            "'InNvZWdpZGV2LmlkQGdtYWlsLmNvbSI.YyBGlQ._N8t6mNnf876WoeSBON9vPZO1MI',"
            "'2022-09-13 16:08:17.243137','2022-09-03 10:57:49.315629','2022-09-13 16:08:17.243137');")
conn.commit()
cur.close()
conn.close()