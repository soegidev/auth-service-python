"""API Blueprint"""
from flask import Blueprint, request, json, session
from core import service_login
from core import service_register
from core import email_confirmation
from core import get_user_show
from core.middleware.authenticate import token_required, verified_required
from .view_response import view_response
api_x = Blueprint("api_x", __name__)


# @api_x.route("/")
# def index():
#     """index_api_blueprint"""
#     return redirect(url_for('form_x.index'))

@api_x.route('/')
def hello_world():
    """hello world"""
    return {'message': 'Hello Flask!'}


@api_x.route("/sign_in", methods=["POST"])
def sign_in():
    """Sign_In"""
    payload = json.loads(request.data)
    param = payload
    result = service_login(**param)
    print(result)
    return view_response(result)


@api_x.route("/sign_up", methods=["POST"])
def sign_up():
    """Sign_Up"""
    payload = json.loads(request.data)
    param = payload
    result = service_register(**param)
    status = result['status']
    if status is True:
        email = result['data']['email']
        session['email'] = email
        email_confirmation(email=email)
    return view_response(result)


@api_x.route("/show", methods=["GET"])
@token_required
@verified_required
def user_show(current_user):
    """get_user_show"""
    print(f"USER_LOGIN {current_user.user_id}")
    user_id = request.args.get('id')
    result = get_user_show(user_id)
    return view_response(result)
