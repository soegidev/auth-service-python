import os
class Config(object):
    TESTING = False
    MAIL_SERVER = "mail.simp.co.id"
    MAIL_PORT = 587
    MAIL_USERNAME = os.environ['MAIL_USERNAME'] # "dwi.fajar@simp.co.id"
    MAIL_PASSWORD = os.environ['MAIL_PASSWORD'] # "corona2019!"


class DevelopmentConfig(Config):
    TESTING = False
    DEBUG = True
    ENV = "Development"
    HOST = "103.187.146.183"
    USER = "postgres"
    DB = "db_user_module"
    PWD = "fajarsoegi1901"
    PORT = 5432
    T_REFRESH = "refresh"
    SECRET_KEY = "xblocks2022"
    SECRET_KEY_REFRESH = "xblocks2022_reload"
    JWT_ACCESS_TOKEN_EXPIRES = 1
    JWT_REFRESH_TOKEN_EXPIRES = 30
    SECURITY_PASSWORD_SALT = 'my_zhafran'


class TestingConfig(Config):
    TESTING = True
    ENV = "Testing"
    ENV = "Development"
    HOST = "103.187.146.183"
    USER = "postgres"
    DB = "db_user_module"
    PWD = "fajarsoegi1901"
    PORT = 5432
    T_REFRESH = "refresh"
    SECRET_KEY = "xblocks2022"
    SECRET_KEY_REFRESH = "xblocks2022_reload"
    JWT_ACCESS_TOKEN_EXPIRES = 1
    JWT_REFRESH_TOKEN_EXPIRES = 30
    SECURITY_PASSWORD_SALT = 'my_zhafran'

class ProductionConfig(Config):
    TESTING = False
    DEBUG = False
    ENV = "Production"
    HOST = os.environ['DB_HOST']
    USER = os.environ['DB_USERNAME']
    DB = os.environ['DB_NAME']
    PWD = os.environ['DB_PASSWORD']
    PORT = os.environ['DB_PORT']
    SECURITY_PASSWORD_SALT = os.environ['SECURE_SALT'] # my_zhafran
    SECRET_KEY = os.environ['SECRET_KEY'] # 19018816062022
    T_REFRESH = "refresh"
    SECRET_KEY_REFRESH = "xblocks2022_reload"
    JWT_ACCESS_TOKEN_EXPIRES = 24 # HOURS
    JWT_REFRESH_TOKEN_EXPIRES = 30 # DAYS
