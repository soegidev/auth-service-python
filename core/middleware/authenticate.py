from flask import jsonify, request, current_app
from functools import wraps
import jwt
from ..model.user_data import SetLogin
from ..connection_db import connection_db


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        conn, cursor = connection_db()
        current_user = SetLogin("User Login")
        token = None
        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']
            print(token)
        if not token:
            return jsonify({'message': 'a valid token is missing'})
        try:
            data = jwt.decode(token, current_app.config['SECRET_KEY'],
                              algorithms=["HS256"])
            query_check_username = "SELECT * FROM public.user_login_data a \
                inner join user_account b on a.id = b.id WHERE a.id=%s"
            sql_where = (data['id'],)
            cursor.execute(query_check_username, sql_where)
            if cursor.rowcount == 0:
                return jsonify({'message': 'User Not Exist in Application'})
            else:
                row = cursor.fetchone()
                current_user.set_login_data(row)
        except Exception as e:
            message = f'token is invalid {str(e)}'
            return jsonify({'message': message})
        return f(current_user, *args, **kwargs)
    return decorator


def verified_required(func):
    conn, cursor = connection_db()

    def inner(*args, **kwargs):
        token = None
        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']
            print(token)
        if not token:
            return jsonify({'message': 'a valid token is missing'})
        try:
            data = jwt.decode(token, current_app.config['SECRET_KEY'],
                              algorithms=["HS256"])
            query_check_username = "SELECT * FROM public.user_login_data \
                    a inner join user_account b on a.id = b.id WHERE a.id=%s"
            sql_where = (data['id'],)
            cursor.execute(query_check_username, sql_where)
            if cursor.rowcount == 0:
                return jsonify({
                    'message': 'User Not Exist in Application'})
            else:
                row = cursor.fetchone()
                if row['email_validation_status_id'] == 2:
                    return jsonify({
                        'message': "The account won't be verified"
                        }), 401
        except Exception as e:
            message = f'token is invalid {str(e)}'
            return jsonify({'message': message})
        return func(*args, **kwargs)
    return inner
