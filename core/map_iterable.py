"""Map Iterable Data"""


def map_iterable(status, error, message, data, meta=None):
    """map_iterable"""
    data = {'status': status, 'error': error, 'message': message,
            'data': data, 'meta': meta}
    return data
