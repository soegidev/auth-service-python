"""User Login"""
from ..connection_db import connection_db
from ..map_iterable import map_iterable
from ..model.user_data import SetLogin


def login(**kwargs):
    """Login User"""
    set_login_data = SetLogin("User Login")
    try:
        conn, cursor = connection_db()
        required_fields = ["username", "password"]
        for field in required_fields:
            if field not in kwargs:
                message = f'{field}is required'
                return map_iterable(False, False, message, None)
        username = kwargs["username"]
        password = kwargs["password"]
        query = "SELECT * FROM public.user_login_data a \
            inner join user_account b on a.id =b.id WHERE username=%s"
        sql_where = (username,)
        conn.cursor()
        cursor.execute(query, sql_where)
        if cursor.rowcount == 0:
            message = "Username Not Exist"
            return map_iterable(False, False, message, None)
        row = cursor.fetchone()
        print(row['hashalgorithm_id'])
        print(row['password_hash'])
        set_login_data.hash(row['hashalgorithm_id'], row['password_hash'])
        set_login_data.set_login_data(row)
        _algoid = set_login_data.hash_algorithm_id
        if set_login_data.check_password(password) is False:
            message = "Password Not Valid"
            return map_iterable(False, False, message, None)
        query = "SELECT * FROM public.hashing_algorithms WHERE id=%s"
        sql_where = (_algoid,)
        cursor.execute(query, sql_where)
        row2 = cursor.fetchone()
        set_login_data.set_algorithm_model(row2['description'])
        return set_login_data.format_login()
    except ZeroDivisionError:
        pass
    return None
