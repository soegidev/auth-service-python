"""Forgot Password"""
import datetime
from flask import url_for, render_template
from ..libraries import send_email as email_
from ..map_iterable import map_iterable
from ..connection_db import connection_db
from ..model.user_data import SetRegister


def send_email_register(**kwargs):
    try:
        required_fields = ["email"]
        for field in required_fields:
            if field not in kwargs:
                message = f'{field} is required'
                return map_iterable(False, False, message, None)
        email = kwargs["email"]
        kirim_email = None
        if kwargs['resend'] is True:
            kirim_email = send_email_confirmation(email)
        if kirim_email['status'] is False:
            message = f"{ kirim_email['message']}"
            return map_iterable(False, True, message, None)
        data = {'email': email}
        message = "Please Check Your Email"
        return map_iterable(True, False, message, data)
    except ZeroDivisionError:
        pass


def resend_forgot_password(email_string):
    """Resend Forgot password"""
    try:
        print(email_string)
        token = email_.generate_confirmation_token(email_string)
        confirm_url = url_for('form_x.forgot_password',
                              token=token, _external=True)
        html = render_template('user/forgot_pwd.html', confirm_url=confirm_url)
        subject = "Change Password Link"
        email_.send_email(email_string, subject, html)
        message = "Message sent!"
        return map_iterable(True, False, message, None)
    except ZeroDivisionError:
        pass


# Send Email Confirmation
def send_email_confirmation(email_string):
    """Send Email Confirmation"""
    try:
        token = email_.generate_confirmation_token(email_string)
        confirm_url = url_for('form_x.confirm_email',
                              token=token, _external=True)
        html = render_template('user/activate.html', confirm_url=confirm_url)
        subject = "Please confirm your email"
        sending = email_.send_email(email_string, subject, html)
        message = f"{sending}"
        return map_iterable(True, False, message, None)
    except ZeroDivisionError:
        pass


def password_confirmation(**kwargs):
    conn, cursor = connection_db()
    try:
        token = kwargs['token']
        email = email_.confirm_token(token)
        if email is False:
            message = "The confirmation link is invalid or has expired."
            return map_iterable(False, True, message, None)
    except Exception:
        message = "The confirmation link is invalid or has expired."
        return map_iterable(False, True, message, None)
    query_check_email = "SELECT * FROM public.user_login_data a\
        inner join user_account b on a.id =b.id WHERE a.emailaddress=%s"
    sql_where = (email,)
    cursor.execute(query_check_email, sql_where)
    if cursor.rowcount == 0:
        message = "Email Not Exist"
        return map_iterable(False, False, message, None)
    row = cursor.fetchone()
    email = row['emailaddress']
    user_register = SetRegister("User Registration")
    user_register.set_password_salt_new()
    user_register.set_password_hash_new(kwargs['password'])
    user_register.generate_hash_algorithms_id()
    password_hash = user_register.password_hash.decode('utf-8')
    password_salt = user_register.password_salt.decode('utf-8')
    createddate = datetime.datetime.now()
    updated_rows = 0
    update_query = "UPDATE user_login_data set password_hash = %s,\
        password_salt=%s, updateddate =%s , password_recovery_token=%s,\
            recovery_token_time=%s WHERE emailaddress=%s"
    sql_where = (password_hash, password_salt,
                 createddate, token, createddate, email)
    cursor.execute(update_query, sql_where)
    updated_rows = cursor.rowcount
    if updated_rows > 0:
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cursor.close()
        data = {'token': token, 'email': email}
        message = "You have confirmed your account. Thanks!'"
        return map_iterable(True, False, message, data)
    data = {'token': token, 'email': email}
    message = "Problem"
    return map_iterable(True, False, message, data)
