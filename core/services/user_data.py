"""User Data"""
import datetime
from ..connection_db import connection_db
from ..map_iterable import map_iterable
from ..model.user_data import SetUserData
from ..libraries import paginate


def get_list(**kwargs):
    try:
        conn, cursor = connection_db()
        if kwargs['search'] is None:
            print("NONE")
            query = "select a.*,b.*,c.externalprovideid as externalprovideid,\
                c.externalprovidetoken,d.description as role_description,\
                    e.description as hashalgorithm_description\
                        from user_account a \
                                    left join user_login_data b \
                                        on b.id  = a.id \
                                    left join user_login_data_external c \
                                    on c.id = a.id \
                                    left join user_roles d \
                                    on d.id = a.role_id \
                                    left join hashing_algorithms e \
                                    on e.id = b.hashalgorithm_id\
                                        order by firstname asc"
            cursor.execute(query)
        else:
            print("YES")
            sql_where = ('%', kwargs['search'], '%', '%',
                         kwargs['search'], '%')
            query = "select a.*,b.*,c.externalprovideid as externalprovideid,\
                c.externalprovidetoken,d.description \
                    as role_description,e.description \
                    as hashalgorithm_description  from user_account a \
                                    left join user_login_data \
                                        b on b.id  = a.id \
                                    left join user_login_data_external c \
                                    on c.id = a.id \
                                    left join user_roles d \
                                    on d.id = a.role_id \
                                    left join hashing_algorithms e \
                                    on e.id = b.hashalgorithm_id \
                                    where b.username like %s || %s || %s \
                                        or b.emailaddress like %s || %s || %s \
                                    order by firstname asc"
            cursor.execute(query, sql_where)
        data = []
        if cursor.rowcount == 0:
            message = "Data Not Found"
            return map_iterable(False, False, message, None)
        else:
            list = cursor.fetchall()
            for row in list:
                set = SetUserData("USER GET DATA", row)
                result = set.format_user_data()
                data.append(result)
        page = kwargs['page']
        data_page, meta = paginate(data, page)
        data = [view_data for view_data in data_page]
        message = "User Data Successfully"
        return map_iterable(True, False, message, data, meta)
    except ZeroDivisionError:
        pass


def get_detail(**kwargs):
    try:
        conn, cursor = connection_db()
        if kwargs['id'] is None:
            message = "Key Not Found"
            return map_iterable(False, False, message, None)
        else:
            sql_where = (kwargs['id'],)
            query = "select a.*,b.*,c.externalprovideid as externalprovideid,\
                    c.externalprovidetoken,d.description as role_description,\
                    e.description as hashalgorithm_description \
                        from user_account a \
                        left join user_login_data b \
                        on b.id  = a.id \
                        left join user_login_data_external c \
                        on c.id = a.id \
                        left join user_roles d \
                        on d.id = a.role_id \
                        left join hashing_algorithms e \
                        on e.id = b.hashalgorithm_id \
                        where b.id = %s\
                        order by firstname asc"
            cursor.execute(query, sql_where)
        conn.cursor()
        if cursor.rowcount == 0:
            message = "Data Not Found"
            return map_iterable(False, False, message, None)
        else:
            row = cursor.fetchone()
            set = SetUserData("USER GET DATA", row)
            user = set.format_user_data()
        message = "User Data Successfully"
        data = user
        return map_iterable(True, False, message, data)
    except ZeroDivisionError:
        pass


def user_edit(**kwargs):
    try:
        conn, cursor = connection_db()
        user_id = kwargs['id']
        dateofbirth = kwargs['date_of_birth']
        first_name = kwargs['first_name']
        last_name = kwargs['last_name']
        updated_at = datetime.datetime.now()
        query = "UPDATE user_account set firstname = %s,\
            lastname=%s, dateofbirth=%s, updateddate=%s where id = %s"
        q_where = (first_name, last_name, dateofbirth, updated_at, user_id)
        cursor.execute(query, q_where)
        conn.commit()
        cursor.close()
        conn.close()
        data = {'id': user_id, 'first_name': first_name,
                'last_name': last_name, 'date_of_birth': dateofbirth}
        message = 'Successfully Updated'
        return map_iterable(True, False, message, data)
    except ZeroDivisionError:
        pass
