import datetime
import uuid
from ..connection_db import connection_db
from ..model.user_data import SetRegister
from ..map_iterable import map_iterable


def register(**kwargs):
    try:
        conn, cursor = connection_db()
        user_id = str(uuid.uuid4())
        username = kwargs["username"]
        password = kwargs["password"]
        email = kwargs['email']
        first_name = kwargs['first_name']
        last_name = kwargs['last_name']
        gender = 'm'
        role_id = 1
        deleted = False
        email_validation_status_id = 2
        created_date = datetime.datetime.now()
        user_register = SetRegister("User Registration")
        user_register.set_password_salt_new()
        user_register.set_password_hash_new(password)
        user_register.generate_hash_algorithms_id()
        password_hash = user_register.password_hash.decode('utf-8')
        password_salt = user_register.password_salt.decode('utf-8')
        qr = f"WITH ua as ( \
            INSERT INTO user_account(id,firstname,lastname,\
                gender,role_id,deleted,createddate) \
            VALUES ('{user_id}','{first_name}',\
                '{last_name}','{gender}','{role_id}',\
                '{deleted}','{created_date}') RETURNING *)\
            INSERT INTO user_login_data(id, username,\
                password_hash,password_salt,\
                hashalgorithm_id,emailaddress,\
                 email_validation_status_id,createddate) \
            VALUES ((select ua.id from ua),'{username}',\
                '{password_hash}','{password_salt}',\
                '{user_register.hash_algorithm_id}'\
                ,'{email}',{email_validation_status_id},'{created_date}');"
        cursor.execute(qr)
        conn.commit()
        cursor.close()
        conn.close()
        data = {'email': email, 'username': username}
        message = 'Successfully Registered'
        return map_iterable(True, False, message, data)
    except ZeroDivisionError:
        pass
