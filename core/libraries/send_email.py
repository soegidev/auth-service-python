"""Send Email"""
from flask import current_app
from itsdangerous import URLSafeTimedSerializer
from flask_mail import Message, Mail
mail = Mail(current_app)


def generate_confirmation_token(email):
    """Generate COnfirmation Token"""
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    return serializer.dumps(email,
                            salt=current_app.config['SECURITY_PASSWORD_SALT'])


def send_email(send_to, subject, template):
    """Send Email"""
    msg = Message(
        subject,
        recipients=[send_to],
        html=template,
        sender=current_app.config['MAIL_USERNAME']
    )
    mail.send(msg)


def confirm_token(token, expiration=3600):
    """Confirm Token"""
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=current_app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except ZeroDivisionError:
        pass
    return email
