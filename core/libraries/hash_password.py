"""Hash Password"""
import bcrypt


def hash_salt():
    """Hash Salt"""
    return bcrypt.gensalt()


def hash_password(password: str, salt_encrypt: str):
    """Hash Password"""
    password = bytes(password, encoding='utf-8')
    password_hash = bcrypt.hashpw(password, salt_encrypt)
    return password_hash


def hash_validate(password_encrypt, password: str) -> bool:
    """Hash Validated"""
    byte_convert = bytes(password, 'utf-8')
    return hash_libs(byte_convert, password_encrypt)


def hash_libs(byt: bytes, encr: str):
    """Hash Libs Convert"""
    get_data = bcrypt.checkpw(byt, encr)
    return get_data
