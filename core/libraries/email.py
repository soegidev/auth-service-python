"""Email Libraries"""
import re
from ..connection_db import connection_db


def email(email_string, gate: int = 0):
    """Email"""
    conn, cursor = connection_db()
    dict_email = {}
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    if re.fullmatch(regex, email_string):
        if gate == 0:
            sql_where = (email_string,)
            query = "SELECT * FROM user_login_data WHERE emailaddress=%s"
            conn.cursor()
            cursor.execute(query, sql_where)
            if cursor.rowcount == 1:
                dict_email['status'] = False
                dict_email['error'] = False
                dict_email['message'] = "Email is Existing"
                return dict_email
            dict_email['status'] = True
            dict_email['error'] = False
            dict_email['message'] = "Email Valid"
            return dict_email
        dict_email['status'] = True
        dict_email['error'] = False
        dict_email['message'] = "Email Valid "
        return dict_email
    dict_email['status'] = False
    dict_email['error'] = False
    dict_email['message'] = "Email Not Valid"
    return dict_email
