"""Username Librarie"""
import re
from ..connection_db import connection_db


def username(char_string, gate: int = 0):
    """validation Username"""
    conn, cursor = connection_db()
    dict_username = {}
    regex_username_lower = r"^[a-z][a-z0-9._]{7,29}$"
    while True:
        regex = re.compile(regex_username_lower)
        if len(char_string) > 20:
            dict_username['status'] = False
            dict_username['error'] = False
            dict_username['message'] = 'length should be \
                                        not be greater than 20'
        if regex.match(char_string):
            if gate == 0:
                query = f"SELECT * FROM user_login_data \
                          WHERE username='{char_string}'"
                conn.cursor()
                cursor.execute(query)
                cursor.close()
                if cursor.rowcount == 1:
                    dict_username['status'] = False
                    dict_username['error'] = False
                    dict_username['message'] = "Username is Existing "
                    return dict_username
                dict_username['status'] = True
                dict_username['error'] = False
                dict_username['message'] = "Username Valid "
                return dict_username
            dict_username['status'] = True
            dict_username['error'] = False
            dict_username['message'] = "Username Valid "
            return dict_username
        if len(char_string) < 7:
            dict_username['status'] = False
            dict_username['error'] = False
            dict_username['message'] = 'username length should be at least 7'
            return dict_username
        dict_username['status'] = False
        dict_username['error'] = False
        dict_username['message'] = "Oh no, that's not right. You need only \
                    start lowercase and number and only one _ and . "
        return dict_username
