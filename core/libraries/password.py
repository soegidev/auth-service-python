"""Password Libraries"""


def password(password_string):
    """Password"""
    dict_password = {}
    special_char = r"'~!@#$%^&*()_+=-`"
    status = True
    message = None
    error = False
    if len(password_string) < 6:
        message = 'length should be at least 6'
        status = False
        error = False
    if len(password_string) > 20:
        message = 'length should be not be greater than 20'
        status = False
        error = False
    if not any(char.isdigit() for char in password_string):
        message = 'Password should have at least one numeral'
        status = False
        error = False
    if not any(char.isupper() for char in password_string):
        message = 'Password should have at least one uppercase letter'
        status = False
        error = False
    if not any(char.islower() for char in password_string):
        message = 'Password should have at least one lowercase letter'
        status = False
        error = False
    if not any(char in special_char for char in password_string):
        message = f'Password should have at\
                least one of the symbols {special_char}'
        status = False
        error = False
    else:
        message = 'Password Valid'
        status = True
        error = False
    dict_password['error'] = error
    dict_password['status'] = status
    dict_password['message'] = message
    return dict_password


def confirmation_password(password_real, password_confirmation):
    """Confirmation password"""
    dict_password = {}
    message = None
    val = False
    password1 = password_real
    password2 = password_confirmation
    if password1 and password2 and password1 != password2:
        message = "Password do not match"
        dict_password['error'] = False
        val = False
        print(message)
    else:
        message = "Password match"
        dict_password['error'] = False
        val = True
        print(message)
    dict_password['status'] = val
    dict_password['message'] = message
    return dict_password
