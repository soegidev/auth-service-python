"""Core Libraries"""
from .username import username
from .password import password
from .password import confirmation_password
from .email import email
from ..map_iterable import map_iterable


def validation_form_register(func):
    """Validation Form User"""
    def inner(**kwargs):
        required_fields = ["username", "email", "password",
                           "confirmation_password", "first_name",
                           "last_name"]
        for field in required_fields:
            if field not in kwargs:
                message = f'{field} is required'
                return map_iterable(False, False, message, None)
        return func(**kwargs)
    return inner


def validation_form_login(func):
    """Validation Form Login"""
    def inner(**kwargs):
        required_fields = ["username", "password"]
        for field in required_fields:
            if field not in kwargs:
                message = f'{field} is required'
                return map_iterable(False, False, message, None)
        return func(**kwargs)
    return inner


def validation_user_login(func):
    """Validation User Login"""
    def inner(**kwargs):
        print("Cek Validasi User")
        val1 = username(kwargs['username'], 1)
        val2 = password(kwargs['password'])
        statement = [val1, val2]
        for x_statement in statement:
            if x_statement['status'] is False:
                return x_statement
        return func(**kwargs)
    return inner


def validation_user_register(func):
    """Validation user Register"""
    def inner(**kwargs):
        val1 = username(kwargs['username'], 0)
        val2 = password(kwargs['password'])
        val3 = email(kwargs['email'], 0)
        val4 = confirmation_password(kwargs['password'],
                                     kwargs['confirmation_password'])
        statement = [val1, val2, val3, val4]
        for x_statement in statement:
            if x_statement['status'] is False:
                return x_statement
        if kwargs['first_name'] is None:
            message = "First Name is Required"
            return map_iterable(False, False, message, None)
        if kwargs['last_name'] is None:
            message = "Last Name is Required"
            return map_iterable(False, False, message, None)
        return func(**kwargs)
    return inner


def validation_forgot_password(func):
    """Validaion forgot Password"""
    def inner(**kwargs):
        val2 = password(kwargs['password'])
        val4 = confirmation_password(kwargs['password'],
                                     kwargs['confirmation_password'])
        if val2['status'] is False:
            print("Check Validasi untuk Forgot Password Password")
            return val2
        if val4['status'] is False:
            print("Check Validasi untuk Forgot Password Confirmation Password")
            return val4
        return func(**kwargs)
    return inner


def paginate(items: list, page: int = 1, per_page: int = 10):
    ''' Return a list of paginated items and a dict contains meta data '''
    start_index = (page - 1) * per_page
    end_index = start_index + per_page
    totaldata = len(items)
    pages = divmod(totaldata, per_page)
    meta = {
        'total': len(items),
        'current_page': page,
        'per_page': per_page,
        'total_pages': pages[0] + 1 if pages[1] else pages[0],
        }
    return items[start_index:end_index], meta
