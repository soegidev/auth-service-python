"""Generate Token"""
import datetime
from flask import current_app
import jwt
secret_key = current_app.config.get('SECRET_KEY')
secret_key_refresh = current_app.config.get('SECRET_KEY_REFRESH')
expire_token = current_app.config.get('JWT_ACCESS_TOKEN_EXPIRES')
expired_token_refresh = current_app.config.get('JWT_REFRESH_TOKEN_EXPIRES')


def create(**kwargs):
    """CreateToken"""
    dict_token = {}
    user_id = kwargs["id"]
    algorithm_id = kwargs["algorithm"]
    data_user = {"id": user_id, "algorithm": algorithm_id}
    print(kwargs)
    try:
        exp = datetime.datetime.utcnow()
        +datetime.timedelta(hours=int(expire_token))
        token = create_token(data_user, exp)
        # refresh = create_refresh_token(data_user)
        dict_token['status'] = True
        dict_token['error'] = False
        dict_token['message'] = 'Generate Token has been successfully'
        dict_token['data'] = {"token": token, "exp": exp}
        return dict_token
    except RuntimeError as err:
        dict_token['status'] = False
        dict_token['error'] = True
        dict_token['message'] = f"{err}"
        return dict_token


def create_token(data_user: object, exp=None):
    """CreateToken"""
    token = jwt.encode({
            'id': data_user['id'],
            'exp': exp
        }, secret_key, algorithm=data_user['algorithm'])
    return token


def create_refresh_token(data_user: object):
    """Create Refresh Token"""
    token = jwt.encode({
            'id': data_user['id'],
            'exp': datetime.datetime.utcnow() +
            datetime.timedelta(days=int(expired_token_refresh))
        }, secret_key_refresh, algorithm=data_user['algorithm'])
    return token
