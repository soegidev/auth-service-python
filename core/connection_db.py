"""connection_db"""
import psycopg2
import psycopg2.extras
from flask import current_app


def connection_db():
    """Connection DB"""
    conn = psycopg2.connect(host=current_app.config.get('HOST'),
                            database=current_app.config.get('DB'),
                            user=current_app.config.get('USER'),
                            password=current_app.config.get('PWD'),
                            port=current_app.config.get('PORT'))
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    return conn, cursor
