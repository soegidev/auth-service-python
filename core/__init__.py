"""Service Login"""
from .libraries import validation_form_login
from .libraries import validation_user_login
from .libraries import validation_form_register
from .libraries import validation_user_register
from .libraries import validation_forgot_password
from .libraries import generate_token
from .services.user_login import login
from .services.user_register import register
from .services.user_data import get_detail, user_edit, get_list
from .services.forgot_password import resend_forgot_password
from .services.forgot_password import send_email_register
from .services.forgot_password import password_confirmation


@validation_form_login
@validation_user_login
def service_login(**kwargs):
    """Service Login"""
    login_user = login(**kwargs)
    if login_user['status'] is False:
        return login_user
    kwargs['id'] = login_user['user_id']
    kwargs['algorithm'] = login_user['algorithm']
    token = generate_token.create(**kwargs)
    return token


def service_forgot_password(**kwargs):
    """Service Forgot Password"""
    print(kwargs)
    email = kwargs['email']
    resend_password = resend_forgot_password(email)
    return resend_password


@validation_form_register
@validation_user_register
def service_register(**kwargs):
    """Service Register"""
    register_user = register(**kwargs)
    if register_user['status'] is False:
        return register_user
    return register_user


@validation_forgot_password
def password_confirm(**kwargs):
    """Password Confirm"""
    pass_result = password_confirmation(**kwargs)
    return pass_result


def email_confirmation(**kwargs):
    """Email Confirmation"""
    email_confirm = send_email_register(**kwargs)
    return email_confirm


def get_user_show(user_id):
    """Get User Data"""
    data_user = get_detail(id=user_id)
    return data_user


def get_user_list(search, page):
    """Get User Data"""
    data_user = get_list(search=search, page=page)
    return data_user


def user_update(**kwargs):
    """User Update"""
    get_data = user_edit(**kwargs)
    return get_data
