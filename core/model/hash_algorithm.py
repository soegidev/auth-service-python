"""Hash Algorithm"""
import dataclasses
from ..connection_db import connection_db


@dataclasses.dataclass
class HashAlgorithm:
    """Hash Algorithm"""
    def __init__(self):
        self.hash_id = None
        self.description = None
        self.created_date = None
        self.updated_date = None

    def set_hash_algorithm_id(self, initial):
        """Set Hash Algorithm Id"""
        self.hash_id = initial

    def set_algorithm_name(self, initial):
        """Set Algorithm Name"""
        self.description = initial

    def set_created_date(self, initial):
        """Set Created Date"""
        self.created_date = initial

    def set_updated_date(self, initial):
        """Set Updated Date"""
        self.updated_date = initial

    def get_id(self):
        """Get Id"""
        return self.hash_id

    def __repr__(self):
        return repr(self.__dict__)

    def get_hash_algo_id(self, id_hash: int):
        """Hash Algo Id"""
        conn, cursor = connection_db()
        query = "SELECT * FROM hashing_algorithms where id =%s"
        sql_where = (id_hash,)
        conn.cursor()
        cursor.execute(query, sql_where)
        row = cursor.fetchone()
        self.hash_id = row['id']
        self.description = row['description']
        self.created_date = row['createddate']
        self.updated_date = row['updateddate']
        cursor.close()
        conn.close()

    def format(self):
        """Format"""
        return {"id": self.hash_id,
                "description": self.description,
                "createddate": self.created_date,
                "updateddate": self.updated_date}
