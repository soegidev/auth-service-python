"""User Data"""
import dataclasses
from ..libraries import hash_password
from .hash_algorithm import HashAlgorithm


@dataclasses.dataclass
class UserData:
    """UserData"""
    def __init__(self, app_name):
        self.app_name = app_name

class SetPassword(UserData):
    """Set Password"""
    def __init__(self, app_name):
        UserData.__init__(self, app_name)
        self.password_hash = None
        self.hash_algorithm_id = None

    def hash(self, algo_id, password):
        """Hash"""
        self.hash_algorithm_id = algo_id
        hash_s = bytes(password, encoding='utf-8')
        self.password_hash = hash_s

    def check_password(self, password: str) -> bool:
        """Check Password"""
        return hash_password.hash_validate(self.password_hash, password)


class SetLogin(SetPassword):
    """Set Login"""
    def __init__(self, app_name):
        SetPassword.__init__(self, app_name)
        self.user_id = None
        self.username = None
        self.email_address = None
        self.first_name = None
        self.last_name = None
        self.algorithm_model = None

    def set_algorithm_model(self, initial):
        """Set Algorithm Model"""
        self.algorithm_model = initial

    def set_login_data(self, data):
        """Set Login Data"""
        self.user_id = data['id']
        self.username = data['username']
        self.first_name = data['firstname']
        self.last_name = data['lastname']
        self.email_address = data['emailaddress']

    def format_login(self):
        """Format Login"""
        return {
                "status": True,
                "user_id": self.user_id,
                "username": self.username,
                "email_address": self.email_address,
                'algorithm': self.algorithm_model}


class SetRegister(SetLogin):

    def __init__(self, app_name):
        SetLogin.__init__(self, app_name)

    def set_password_salt_new(self):
        self.password_salt = hash_password.hash_salt()

    def set_password_hash_new(self, password: str):
        self.password_hash = hash_password.hash_password(
            password, self.password_salt)

    def generate_hash_algorithms_id(self):
        ha = HashAlgorithm()
        ha.get_hash_algo_id(1)
        self.hash_algorithm_id = ha.get_id()


class SetUserData(SetLogin):
    def __init__(self, app_name, row):
        super().__init__(app_name)
        self.user_id = row['id']
        self.username = row['username']
        self.first_name = row['firstname']
        self.last_name = row['lastname']
        self.email_address = row['emailaddress']
        self.gender = row['gender']
        self.role_id = row['role_id']
        self.date_of_birth = row['dateofbirth']
        self.role_description = row['role_description']

    def format_user_data(self):
        return {
            "user_id": self.user_id,
            "username": self.username,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email_address": self.email_address,
            "gender": self.gender,
            "role_id": self.role_id,
            "date_of_birth": self.date_of_birth,
            "role_description": self.role_description
        }
