ACCOUNT SERVICE
ORIENTED OBJECT PROGRAMMING (FLASK FRAMEWORK WITH DECORATE)
1. Create VirtualEnv = py -m venv accountEnv => source authEnv/Script/activate
2. Install pip install
    - flask
    - phython-dotenv
    - psycopg2-binary
    - requests
    - pytest
    - pylint
    - pyjwt
    - bcrypt
    - FLASK-MAIL
    - itsdangerous
    - RestPlus
    - Flask apispec
    - marshmallow
    - coverage
3. create package installer = pip freeze > requirements.txt
4. create config.py = touch config.py
5. create .env = touch .env (add FLASK_APP=app)
6. mkdir app,database,model,services,libraries
7. create __init__.py in app
8. mkdir v1 in app
9. create api_blueprint.py in v1
10. create __init__.py in database
11. mkdir view 
12. create viewResp = touch view/__init__.py
13. create validation = mkdir libraries/validation