"""API Blueprint SAMPLE"""
from flask_apispec.views import MethodResource
from flask_apispec import marshal_with, doc, use_kwargs
from flask_restful import Resource, abort
from core import get_user_show
from core import get_user_list
from core import user_update
from marshmallow import Schema, fields
from core.middleware.authenticate import token_required, verified_required
API_ENDPOINT = '/api/user'


class User_Show_Request(Schema):
    id = fields.String(required=True)


class User_Response(Schema):
    message = fields.String()
    error = fields.Boolean(dump_default=False)
    status = fields.Boolean(dump_default=True)
    data = fields.Field()


class List_Request(Schema):
    search = fields.String()
    page = fields.Integer()


class User_Edit_Request(Schema):
    first_name = fields.String()
    last_name = fields.String()
    date_of_birth = fields.String()


class List_Response(Schema):
    message = fields.String()
    error = fields.Boolean(dump_default=False)
    status = fields.Boolean(dump_default=True)
    data = fields.Field()
    meta = fields.Field()


class Show(MethodResource, Resource):
    """UserShow"""
    @doc(description='UserShow', tags=['UserData'], 
    params={'x-access-tokens': {'description': 
    'Authorization HTTP header with JWT access token',
    'in': 'header',
    'type': 'string',
    'required': True}
    })
    # Request
    @use_kwargs(User_Show_Request, location=('query'))
    # Response
    @marshal_with(User_Response)  # marshalling
    @token_required
    def get(self, current_user, **kwargs):
        """get_user_show"""
        user_id = kwargs['id']
        result = get_user_show(user_id)
        if result['status'] is False and result['error'] is False:
            abort(400, message=result['message'])
        if result['status'] is False and result['error'] is True:
            abort(422, message=result['message'])
        return result


class List(MethodResource, Resource):
    """UserShow"""
    @doc(description='UserList', tags=['UserData'], 
    params={'x-access-tokens': {'description':
    'Authorization HTTP header with JWT access token',
    'in': 'header',
    'type': 'string',
    'required': True}
    })
    # Request
    @use_kwargs(List_Request, location=('query'))
    # Response
    @marshal_with(List_Response)  # marshalling
    @token_required
    @verified_required
    def get(self, current_user, **kwargs):
        """get_user_list"""
        result = get_user_list(kwargs.get('search', None),
                               kwargs.get('page', 1))
        if result['status'] is False and result['error'] is False:
            abort(400, message=result['message'])
        if result['status'] is False and result['error'] is True:
            abort(422, message=result['message'])
        return result


class Edit(MethodResource, Resource):
    """UserShow"""
    @doc(description='UserEdit', tags=['UserData'], 
    params={'x-access-tokens': {'description': 
    'Authorization HTTP header with JWT access token',
    'in': 'header',
    'type': 'string',
    'required': True}
    })
    # Request
    @use_kwargs(User_Show_Request, location=('query'))
    @use_kwargs(User_Edit_Request, location=('json'))
    # Response
    @marshal_with(User_Response)  # marshalling
    @token_required
    def put(self, current_user, **kwargs):
        """get_user_edit"""
        data_form = {
            "id": kwargs['id'],
            "first_name": kwargs['first_name'],
            "last_name": kwargs['last_name'],
            "date_of_birth": kwargs['date_of_birth']
            }
        result = user_update(**data_form)
        if result['status'] is False and result['error'] is False:
            abort(400, message=result['message'])
        if result['status'] is False and result['error'] is True:
            abort(422, message=result['message'])
        return result
