"""API Blueprint SAMPLE"""
from flask_apispec.views import MethodResource
from flask_apispec import marshal_with, doc, use_kwargs
from flask_restful import Resource, abort
from core import service_login, service_register
from core import service_forgot_password
from marshmallow import Schema, fields
API_ENDPOINT = "/api"


class Login_Request(Schema):
    username = fields.String(required=True)
    password = fields.String(required=True)


class Register_Request(Schema):
    username = fields.String(required=True)
    password = fields.String(required=True)
    confirmation_password = fields.String(required=True)
    first_name = fields.String(required=True)
    last_name = fields.String(required=True)
    email = fields.String(required=True)


class Auth_Response(Schema):
    message = fields.String()
    error = fields.Boolean(dump_default=False)
    status = fields.Boolean(dump_default=True)
    data = fields.Field()


class Forgot_Password_Request(Schema):
    email = fields.String(required=True)


class Login(MethodResource, Resource):
    """Class SignIn"""
    @doc(description='Auth Login', tags=['Auth'])
    # Request
    @use_kwargs(Login_Request, location=('json'))
    # Response
    @marshal_with(Auth_Response)  # marshalling
    def post(self, **kwargs):
        """User SignIn"""
        result = service_login(**kwargs)
        if result['status'] is False and result['error'] is False:
            abort(400, message=result['message'])
        if result['status'] is False and result['error'] is True:
            abort(422, message=result['message'])
        return result


class Register(MethodResource, Resource):
    """Class SignUp"""
    @doc(description='Auth Register', tags=['Auth'])
    # Request
    @use_kwargs(Register_Request, location=('json'))
    # Response
    @marshal_with(Auth_Response)  # marshalling
    def post(self, **kwargs):
        """Post SignUp"""
        result = service_register(**kwargs)
        if result['status'] is False and result['error'] is False:
            abort(400, message=result['message'])
        if result['status'] is False and result['error'] is True:
            abort(422, message=result['message'])
        return result


class Forgot_Password(MethodResource, Resource):
    """Class SignIn"""
    @doc(description='Auth Login', tags=['Auth'])
    # Request
    @use_kwargs(Forgot_Password_Request, location=('json'))
    # Response
    @marshal_with(Auth_Response)  # marshalling
    def post(self, **kwargs):
        """User SignIn"""
        result = service_forgot_password(**kwargs)
        if result['status'] is False and result['error'] is False:
            abort(400, message=result['message'])
        if result['status'] is False and result['error'] is True:
            abort(422, message=result['message'])
        return result
