"""API Blueprint SAMPLE"""
from flask import Blueprint, current_app
from flask_apispec.extension import FlaskApiSpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec import APISpec
from flask_restful import Api
from .resources import Auth, User
api_bp = Blueprint('api_bp', __name__)
foo_api = Api(current_app)
current_app.config.update({
    'APISPEC_SPEC': APISpec(
        title='USER API SERVICE',
        version='v1',
        plugins=[MarshmallowPlugin()],
        openapi_version='2.0.0'
    ),
    'APISPEC_SWAGGER_URL':
    '/api/swagger/',  # URI to access API Doc JSON
    'APISPEC_SWAGGER_UI_URL':
    '/api/swagger-ui/'  # URI to access UI of API Doc
})
docs = FlaskApiSpec(current_app)


foo_api.add_resource(Auth.Login, f"{Auth.API_ENDPOINT}/login")
docs.register(Auth.Login)
foo_api.add_resource(Auth.Register,
                     f"{Auth.API_ENDPOINT}/register")
docs.register(Auth.Register)
foo_api.add_resource(Auth.Forgot_Password,
                     f"{Auth.API_ENDPOINT}/forgot_password")
docs.register(Auth.Forgot_Password)
foo_api.add_resource(User.Show, f"{User.API_ENDPOINT}/show")
docs.register(User.Show)
foo_api.add_resource(User.List, f"{User.API_ENDPOINT}/list")
docs.register(User.List)
foo_api.add_resource(User.Edit, f"{User.API_ENDPOINT}/edit")
docs.register(User.Edit)
