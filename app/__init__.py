"""App init"""
import os
from flask import Flask
from flask.logging import create_logger


def create_app(test_config=None):
    """Create a new app instance."""
    # pylint: disable=W0621
    app = Flask(__name__)
    app.logger = create_logger(app)
    # app.config.from_mapping(
    #     SECRET_KEY='dev',
    #     DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    # )
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
        if os.environ.get('ENV') == 'development':
            app.config.from_object('config.DevelopmentConfig')
        else:
            app.config.from_object('config.ProductionConfig')
    else:
        app.config.from_pyfile('config.py', silent=True)
        app.config.from_object(test_config)
        # app.config.from_mapping(test_config)
    print(test_config)
    env_p = app.config.get('ENV')
    print(env_p)
    # load the test config if passed in
    # pylint: disable=C0415, W0611
    with app.app_context():
        from .api_blueprint import api_bp
        app.register_blueprint(api_bp)
        from .form_blueprint import form_x
        app.register_blueprint(form_x, url_prefix='/form')
    return app


if __name__ == "__main__":
    app = create_app()
    app.run()
