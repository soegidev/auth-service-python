"""Example web view for application factory."""
import jwt
from flask import Blueprint, current_app, request, session
from flask import render_template, url_for, redirect
from core import connection_db
from core import service_login
from core import service_forgot_password
from core import service_register
from core import email_confirmation
from core import password_confirm
from core import get_user_show
from core import user_update
form_x = Blueprint("form_x", __name__)


@form_x.route('/')
def index():
    """index_form_blueprint"""
    msg = None
    if session.get("loggedin") is True:
        msg = f"Welcome Back! {session['username']}"
        if session.get("validation") == 2:
            msg = "AKun anda belum verifikasi"
        return render_template('index.html', msg=msg)
    msg = "Silahkan Login"
    return render_template('user/login.html', msg=msg)


@form_x.route('/login', methods=['GET', 'POST'])
def login():
    """Login"""
    conn, cursor = connection_db.connection_db()
    msg = ''
    if session.get('loggedin') is True:
        msg = f"Welcome Back! {session['username']}"
        return render_template('index.html', msg=msg)
    if request.method == 'POST' and 'username' \
                         in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']
        param = {"username": username, "password": password}
        result = service_login(**param)
        if result['status'] is True:
            data = jwt.decode(result['data']['token'],
                              current_app.config['SECRET_KEY'],
                              algorithms=["HS256"])
            query_check_username = "SELECT * FROM public.user_login_data \
                a inner join user_account b on a.id = b.id WHERE a.id=%s"
            sql_where = (data['id'],)
            conn.cursor()
            cursor.execute(query_check_username, sql_where)
            row = cursor.fetchone()
            session['loggedin'] = True
            session['id'] = data['id']
            session['username'] = row['username']
            session['validation'] = row['email_validation_status_id']
            session['email'] = row['emailaddress']
            if row['email_validation_status_id'] == 2:
                msg = "Akun anda Belum Verifikasi"
            msg = 'Logged in successfully !'
            # return render_template('index.html', msg = msg)
            return redirect(url_for('form_x.index', msg=msg))
        msg = 'Incorrect username / password !'
    return render_template('user/login.html', msg=msg)


@form_x.route("/resend_forgot_password", methods=['GET', 'POST'])
def resend_forgot_password():
    """resend_forgot_password"""
    msg = ''
    if request.method == 'POST' and 'email_address' in request.form:
        try:
            email = request.form['email_address']
            data_form = {"email": email}
            result = service_forgot_password(**data_form)
            if result['status'] is True:
                msg = "Check Link in Your Mail"
            else:
                msg = result['message']
        except ZeroDivisionError:
            pass
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template("user/form_email_forgot.html", msg=msg)


@form_x.route('/register', methods=['GET', 'POST'])
def register():
    """register"""
    msg = ''
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        confirmation_password = password
        email = request.form['email']
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        gender = 'm'
        role_id = 1
        data_form = {"username": username,
                     "password": password,
                     "confirmation_password": confirmation_password,
                     "email": email,
                     "first_name": first_name,
                     "last_name": last_name,
                     "gender": gender,
                     "role_id": role_id}
        param = data_form
        try:
            result = service_register(**param)
            if result['status'] is True:
                msg = f" {result['message']} Please Login"
                session['email'] = result['data']['email']
                email = session.get("email")
                email_confirmation(email=email)
            msg = f" {result['message']}"
        except ZeroDivisionError:
            pass
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template('user/register.html', msg=msg)


@form_x.route('/forgot_password/<token>', methods=['GET', 'POST'])
def forgot_password(token):
    """forgot_password"""
    msg = ''
    session['token'] = token
    if request.method == 'POST':
        password = request.form['password']
        confirmation_password = request.form['confirmation_password']
        data_form = {
            "token": token, "password": password,
            "confirmation_password": confirmation_password
            }
        try:
            result = password_confirm(**data_form)
            if result['status'] is True:
                msg = "Thanks For Changed Password"
                return render_template(
                    'user/change_password.html',
                    msg=msg, email=result['data']['email'], login=True)
            msg = result['message']
        except ZeroDivisionError:
            pass
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template('user/change_password.html', msg=msg, login=False)


@form_x.route("/user_show", methods=["GET", "POST"])
def user_show():
    """user_show"""
    msg = ''
    user_id = request.args.get('id')
    result = get_user_show(user_id)
    if result['status'] is True:
        data = result['data']
        msg = request.args.get('msg')
    return render_template('user/user_show.html', data=data, msg=msg)


@form_x.route('/save_edit', methods=['GET', 'POST'])
def save_edit():
    """save_edit"""
    msg = ''
    if request.method == 'POST':
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        date_of_birth = request.form['date_of_birth']
        edit_id = request.args.get('id')
        data_form = {
            "id": edit_id,
            "first_name": first_name,
            "last_name": last_name,
            "date_of_birth": date_of_birth
            }
        try:
            result = user_update(**data_form)
            if result['status'] is True:
                msg = "Successfully"
                return redirect(url_for('form_x.user_show',
                                        id=edit_id, msg=msg))
        except ZeroDivisionError:
            pass
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template('user/user_show.html', msg=msg)


# logout #
@form_x.route('/logout')
def logout():
    """"Logout"""
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
    session.pop('email', None)
    session.pop('validation', None)
    return redirect(url_for('form_x.login'))
