import os
import pytest
from app import create_app
from config import TestingConfig


@pytest.fixture()
def app():
    app = create_app(TestingConfig)
    app.config.update({
        "TESTING": True,
    })
    # assert app.config['ENV'] == 'prod'
    # assert not app.config['DEBUG']

    # other setup can go here
    return app

    # clean up / reset resources here


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()
